package com.RAWGApiReceiver.Feign.Response;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GameResponse {
    private Integer id;
    private String name;
    private List<GenreReponse> genres;
    private String background_image;
}
