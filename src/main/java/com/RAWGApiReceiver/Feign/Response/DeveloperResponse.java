package com.RAWGApiReceiver.Feign.Response;

import lombok.*;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DeveloperResponse {
    private Integer id;
    private String name;
}