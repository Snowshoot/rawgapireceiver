package com.RAWGApiReceiver.Feign.Response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class StoreUrlResponse {
    private Integer id;
    private StoreResponse store;
    private String url;
}
