package com.RAWGApiReceiver.Feign.Response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class GenericResponse<T> {
    private Integer count;
    private String next;
    private String previous;
    private List<T> results;
}
