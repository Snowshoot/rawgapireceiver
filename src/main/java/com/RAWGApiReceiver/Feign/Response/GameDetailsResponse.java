package com.RAWGApiReceiver.Feign.Response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GameDetailsResponse {
    private String released;
    private Boolean tba;
    private String updated;
    private String description;
    private List<StoreUrlResponse> stores;
    private List<DeveloperResponse> developers;
    private List<PublisherResponse> publishers;
}
