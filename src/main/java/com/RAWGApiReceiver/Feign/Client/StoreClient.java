package com.RAWGApiReceiver.Feign.Client;

import com.RAWGApiReceiver.Feign.Response.GenericResponse;
import com.RAWGApiReceiver.Feign.Response.StoreResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@FeignClient(name = "rawgStores", url = "https://api.rawg.io/api/")
public interface StoreClient {
    @RequestMapping(method = RequestMethod.GET, value = "/stores")
    GenericResponse<StoreResponse> getStores(@RequestHeader(value = "User-Agent") String appName, @RequestParam Integer page, @RequestParam(name = "page_size") Integer pageSize);
}