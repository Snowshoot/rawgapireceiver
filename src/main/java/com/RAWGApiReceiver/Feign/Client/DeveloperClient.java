package com.RAWGApiReceiver.Feign.Client;

        import com.RAWGApiReceiver.Feign.Response.DeveloperResponse;
        import com.RAWGApiReceiver.Feign.Response.GenericResponse;
        import org.springframework.cloud.openfeign.FeignClient;
        import org.springframework.web.bind.annotation.RequestHeader;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.RequestMethod;
        import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "rawgDevelopers", url = "https://api.rawg.io/api/")
public interface DeveloperClient {
    @RequestMapping(method = RequestMethod.GET, value = "/developers")
    GenericResponse<DeveloperResponse> getDevelopers(@RequestHeader(value = "User-Agent") String appName, @RequestParam Integer page, @RequestParam(name = "page_size") Integer pageSize);
}