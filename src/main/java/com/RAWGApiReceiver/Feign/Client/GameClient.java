package com.RAWGApiReceiver.Feign.Client;

import com.RAWGApiReceiver.Feign.Response.GameDetailsResponse;
import com.RAWGApiReceiver.Feign.Response.GameResponse;
import com.RAWGApiReceiver.Feign.Response.GenericResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;


@FeignClient(name = "rawgGames", url = "https://api.rawg.io/api/")
public interface GameClient {
    @RequestMapping(method = RequestMethod.GET, value = "/games")
    GenericResponse<GameResponse> getGames(@RequestHeader(value = "User-Agent") String appName,
                                           @RequestParam(name="page") Integer page,
                                           @RequestParam(name = "page_size") Integer pageSize,
                                           @RequestParam(name="platforms") Integer platforms,
                                           @RequestParam(name="dates") String dates,
                                           @RequestParam(name = "exclude_additions") Boolean excludeAdditions);

    @RequestMapping(method =RequestMethod.GET, value = "/games/{id}")
    GameDetailsResponse getGameDetails(@RequestHeader(value = "User-Agent") String appName, @PathVariable("id") Integer id);
}