package com.RAWGApiReceiver.Feign.Client;

import com.RAWGApiReceiver.Feign.Response.GenericResponse;
import com.RAWGApiReceiver.Feign.Response.GenreReponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@FeignClient(name = "rawgGenres", url = "https://api.rawg.io/api/")
public interface GenreClient {
    @RequestMapping(method = RequestMethod.GET, value = "/genres")
    GenericResponse<GenreReponse> getGenres(@RequestHeader(value = "User-Agent") String appName, @RequestParam Integer page, @RequestParam(name = "page_size") Integer pageSize);
}