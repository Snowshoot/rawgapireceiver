package com.RAWGApiReceiver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class RawgApiReceiverApplication {
	public static void main(String[] args) {
		SpringApplication.run(RawgApiReceiverApplication.class, args);
	}
}
