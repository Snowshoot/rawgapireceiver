package com.RAWGApiReceiver.Service.RequestHistory;

import com.RAWGApiReceiver.Entity.RequestHistoryEntity;
import com.RAWGApiReceiver.Repository.RequestHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class RequestHistoryService {
    @Autowired
    private RequestHistoryRepository requestHistoryRepository;

    public void save(RequestHistoryEntity request){
        requestHistoryRepository.save(request);
    }

    public void delete(RequestHistoryEntity request){requestHistoryRepository.delete(request);}

}
