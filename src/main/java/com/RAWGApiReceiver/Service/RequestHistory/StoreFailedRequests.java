package com.RAWGApiReceiver.Service.RequestHistory;

import com.RAWGApiReceiver.Entity.RequestHistoryEntity;
import com.RAWGApiReceiver.Entity.RequestTypeEnum;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StoreFailedRequests {
    @Autowired
    RequestHistoryService requestHistoryService;
    ObjectMapper objectMapper;

    public void storeFailedRequest(Object request, RequestTypeEnum requestTypeEnum){
                try {
                    RequestHistoryEntity requestHistory = new RequestHistoryEntity(objectMapper.writeValueAsString(request), requestTypeEnum);
                    requestHistoryService.save(requestHistory);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
        }
    }