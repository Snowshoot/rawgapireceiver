package com.RAWGApiReceiver.Service.Utils;

import org.springframework.stereotype.Service;
import java.time.LocalDate;

@Service
public class DateUtil {
    public String createCurrentDates(){
        LocalDate dateNow = LocalDate.now();
        LocalDate dateThen = dateNow.plusYears(10);
        return dateNow + "," + dateThen;
    }
}
