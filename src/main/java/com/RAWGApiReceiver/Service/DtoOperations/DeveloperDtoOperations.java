package com.RAWGApiReceiver.Service.DtoOperations;

import com.RAWGApiReceiver.Feign.Client.DeveloperClient;
import com.RAWGApiReceiver.Feign.Response.DeveloperResponse;
import com.RAWGApiReceiver.Feign.Response.GenericResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeveloperDtoOperations {
    @Autowired
    private DeveloperClient developerClient;

    public List<DeveloperResponse> getDevelopers(Integer page, Integer pageSize) { //EXCEPTIONS server down?
        GenericResponse<DeveloperResponse> response = developerClient.getDevelopers("GameReleaseRadarApp", page, pageSize);
        return response.getResults();
    }
}
