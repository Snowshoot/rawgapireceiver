package com.RAWGApiReceiver.Service.DtoOperations;

import com.RAWGApiReceiver.Feign.Client.GenreClient;
import com.RAWGApiReceiver.Feign.Response.GenericResponse;
import com.RAWGApiReceiver.Feign.Response.GenreReponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GenreDtoOperations {
    @Autowired
    GenreClient genreClient;

    public List<GenreReponse> getGenres(Integer page, Integer pageSize){
        GenericResponse<GenreReponse> response = genreClient.getGenres("GameReleaseRadarApp", page, pageSize);
        return response.getResults();
    }
}
