package com.RAWGApiReceiver.Service.DtoOperations;

import com.RAWGApiReceiver.Feign.Client.GameClient;
import com.RAWGApiReceiver.Feign.Response.GameDetailsResponse;
import com.RAWGApiReceiver.Feign.Response.GameResponse;
import com.RAWGApiReceiver.Feign.Response.GenericResponse;
import com.RAWGApiReceiver.Rest.DTO.GameDTO;
import com.RAWGApiReceiver.Rest.DTO.StoreDTO;
import com.RAWGApiReceiver.Service.Utils.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class GameDtoOperations {
    @Autowired
    GameClient gameClient;
    @Autowired
    private DateUtil dateUtil;

    public List<GameDTO> getGames(Integer page, Integer pageSize){
        GenericResponse<GameResponse> response = gameClient.getGames("GameReleaseRadarApp", page, pageSize, 4, dateUtil.createCurrentDates(),true);
        return gameResponseToDto(response.getResults());
    }

    private List<GameDTO> gameResponseToDto(List<GameResponse> gameResponse) {
        List<GameDTO> mappedGames = gameResponse.stream().map(game -> {
                    GameDetailsResponse gameDetails = gameClient.getGameDetails("GameReleaseRadarApp", game.getId());
                    return mapGame(game, gameDetails);
                }
        ).collect(Collectors.toList());
        return mappedGames;
    }

    private GameDTO mapGame(GameResponse gameResponse, GameDetailsResponse gameDetails){
        LocalDate releaseDate = null;
        List<StoreDTO> stores = gameDetails.getStores().stream()
                .filter(store -> store.getStore().getId() == 1 || store.getStore().getId() == 5 || store.getStore().getId() == 9 || store.getStore().getId() == 11)
                .map(store -> new StoreDTO(store.getId(), store.getUrl(), store.getStore().getId(), store.getStore().getName())).collect(Collectors.toList());
        if(gameDetails.getReleased() != null) {
            releaseDate = LocalDate.parse(gameDetails.getReleased());
        }
        return new GameDTO(gameResponse.getId(), gameResponse.getName(), gameDetails.getDescription(), gameResponse.getGenres(), stores, gameDetails.getDevelopers(), gameDetails.getPublishers(), gameResponse.getBackground_image(), releaseDate, gameDetails.getTba());
    }
}
