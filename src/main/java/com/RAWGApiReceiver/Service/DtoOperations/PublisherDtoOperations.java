package com.RAWGApiReceiver.Service.DtoOperations;

import com.RAWGApiReceiver.Feign.Client.PublisherClient;
import com.RAWGApiReceiver.Feign.Response.GenericResponse;
import com.RAWGApiReceiver.Feign.Response.PublisherResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PublisherDtoOperations {
    @Autowired
    PublisherClient publisherClient;

    public List<PublisherResponse> getPublishers(Integer page, Integer pageSize){ //EXCEPTIONS server down?
        GenericResponse<PublisherResponse> response = publisherClient.getPublishers("GameReleaseRadarApp", page, pageSize);
        return response.getResults();
    }
}
