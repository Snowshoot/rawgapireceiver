package com.RAWGApiReceiver.Service.DtoOperations;

import com.RAWGApiReceiver.Feign.Client.StoreClient;
import com.RAWGApiReceiver.Feign.Response.GenericResponse;
import com.RAWGApiReceiver.Feign.Response.StoreResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StoreDtoOperations {
    @Autowired
    private StoreClient storeClient;

    public List<StoreResponse> getStores(Integer page, Integer pageSize){ //EXCEPTIONS server down?
        GenericResponse<StoreResponse> response = storeClient.getStores("GameReleaseRadarApp", page, pageSize);
        return response.getResults();
    }
}
