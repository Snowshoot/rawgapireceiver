package com.RAWGApiReceiver.Repository;

import com.RAWGApiReceiver.Entity.RequestHistoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestHistoryRepository extends JpaRepository<RequestHistoryEntity, Integer> {
}
