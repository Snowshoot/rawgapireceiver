package com.RAWGApiReceiver.Entity;

import lombok.*;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "request_history", uniqueConstraints = {
        @UniqueConstraint(columnNames = "request_history_id")
})
public class RequestHistoryEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "request_history_id", unique = true, nullable = false)
    private Integer requestHistoryId;

    @Column(name = "body", nullable = false)
    @NonNull private String body;

    @Enumerated(EnumType.STRING)
    @Column(name = "request_type", nullable = false)
    @NonNull private RequestTypeEnum requestType;  //enum for deserializtion
}