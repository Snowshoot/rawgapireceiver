package com.RAWGApiReceiver.Rest.DTO;

import com.RAWGApiReceiver.Feign.Response.DeveloperResponse;
import com.RAWGApiReceiver.Feign.Response.PublisherResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import com.RAWGApiReceiver.Feign.Response.GenreReponse;

import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class GameDTO {
    private Integer id;
    private String name;
    private String description;
    private List<GenreReponse> genres;
    private List<StoreDTO> stores;
    private List<DeveloperResponse> developers;
    private List<PublisherResponse> publishers;
    private String cover;
    private LocalDate released;
    private Boolean tba;
}
