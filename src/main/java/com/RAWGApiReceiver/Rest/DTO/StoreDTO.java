package com.RAWGApiReceiver.Rest.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class StoreDTO {
    private Integer storeUrlApiId;
    private String url;
    private Integer storeApiId;
    private String storeName;
}
