package com.RAWGApiReceiver.Rest.Controllers;

import com.RAWGApiReceiver.Rest.DTO.GameDTO;
import com.RAWGApiReceiver.Service.DtoOperations.GameDtoOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class GameController {

    @Autowired
    private GameDtoOperations gameDtoOperations;

    @GetMapping("/games")
    public List<GameDTO> getGames(@RequestParam Integer page, @RequestParam(name = "page_size") Integer pageSize){
        return gameDtoOperations.getGames(page, pageSize);
    }
}

