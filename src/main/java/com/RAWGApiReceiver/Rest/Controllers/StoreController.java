package com.RAWGApiReceiver.Rest.Controllers;

import com.RAWGApiReceiver.Feign.Response.StoreResponse;
import com.RAWGApiReceiver.Service.DtoOperations.StoreDtoOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class StoreController {
    @Autowired
    private StoreDtoOperations storeDtoOperations;

    @GetMapping("/stores")
    public List<StoreResponse> getStores(@RequestParam Integer page, @RequestParam(name = "page_size") Integer pageSize){
        return storeDtoOperations.getStores(page, pageSize);
    }
}