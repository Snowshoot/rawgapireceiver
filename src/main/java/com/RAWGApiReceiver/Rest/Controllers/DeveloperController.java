package com.RAWGApiReceiver.Rest.Controllers;

import com.RAWGApiReceiver.Feign.Response.DeveloperResponse;

import com.RAWGApiReceiver.Service.DtoOperations.DeveloperDtoOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DeveloperController{
    @Autowired
    DeveloperDtoOperations developerDtoOperations;


    @GetMapping("/developers")
    public List<DeveloperResponse> getDevelopers(@RequestParam Integer page, @RequestParam(name = "page_size") Integer pageSize) {
        return developerDtoOperations.getDevelopers(page, pageSize);
    }
}
