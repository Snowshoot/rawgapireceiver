package com.RAWGApiReceiver.Rest.Controllers;

import com.RAWGApiReceiver.Feign.Client.PublisherClient;
import com.RAWGApiReceiver.Feign.Response.GenericResponse;
import com.RAWGApiReceiver.Feign.Response.PublisherResponse;
import com.RAWGApiReceiver.Service.DtoOperations.PublisherDtoOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PublisherController {

    @Autowired
    private PublisherDtoOperations publisherDtoOperations;

    @GetMapping("/publishers")
    public List<PublisherResponse> getPublishers(@RequestParam Integer page, @RequestParam(name = "page_size") Integer pageSize){
        return publisherDtoOperations.getPublishers(page, pageSize);
    }
}
